# ics-ans-role-archiver-exporter

Ansible role to install archiver-exporter.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-archiver-exporter
```

## License

BSD 2-clause
